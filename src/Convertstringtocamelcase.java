public class Convertstringtocamelcase {
    public static void main(String[] args) {
        System.out.println(Convertstringtocamelcase.toCamelCase("The_Stealth_Warrior"));
    }

    static String toCamelCase(String s) {
        String[] sNuevasCadenas = s.split("[-_]");
        StringBuilder sbResult = new StringBuilder(sNuevasCadenas[0]);
        for (int i = 1; i < sNuevasCadenas.length; i++) {
            char cLetter = sNuevasCadenas[i].charAt(0);
            if (cLetter >= 'a' && cLetter <= 'z') {
                sbResult.append(((char) (cLetter - 32)) + sNuevasCadenas[i].substring(1));
            } else {
                sbResult.append(sNuevasCadenas[i]);
            }
        }
        return sbResult.toString();
    }
}

import java.util.HashSet;

public class Arraydiff {
    public static void main(String[] args) {
        for (int i :
                Arraydiff.arrayDiff(new int[]{1, 2}, new int[]{1})) {
            System.out.println(i);
        }
    }

    public static int[] arrayDiff(int[] a, int[] b) {
        HashSet<Integer> hsIndexs = new HashSet<>();
        int[] iResults;
        for (int i :
                b) {
            for (int j = 0; j < a.length; j++) {
                if (i == a[j]) {
                    hsIndexs.add(j);
                }
            }
        }
        iResults = new int[a.length - hsIndexs.size()];
        int iCounter = 0;
        for (int i = 0; i < a.length; i++) {
            if (!hsIndexs.contains(i)) {
                iResults[iCounter++] = a[i];
            }
        }
        return iResults;
    }
}
